package com.timwi.challenge.backend;

import com.google.gson.JsonArray;
import com.neovisionaries.i18n.CountryCode;
import com.timwi.challenge.backend.enums.ModelObjectType;
import com.timwi.challenge.backend.model_objects.specification.Artist;
import com.timwi.challenge.backend.model_objects.specification.Track;
import com.timwi.challenge.backend.requests.authorization.authorization_code.AuthorizationCodeRefreshRequest;
import com.timwi.challenge.backend.requests.authorization.authorization_code.AuthorizationCodeRequest;
import com.timwi.challenge.backend.requests.authorization.authorization_code.AuthorizationCodeUriRequest;
import com.timwi.challenge.backend.requests.authorization.authorization_code.pkce.AuthorizationCodePKCERefreshRequest;
import com.timwi.challenge.backend.requests.authorization.authorization_code.pkce.AuthorizationCodePKCERequest;
import com.timwi.challenge.backend.requests.authorization.client_credentials.ClientCredentialsRequest;
import com.timwi.challenge.backend.requests.data.albums.GetAlbumRequest;
import com.timwi.challenge.backend.requests.data.albums.GetAlbumsTracksRequest;
import com.timwi.challenge.backend.requests.data.albums.GetSeveralAlbumsRequest;
import com.timwi.challenge.backend.requests.data.follow.*;
import com.timwi.challenge.backend.requests.data.follow.legacy.FollowPlaylistRequest;
import com.timwi.challenge.backend.requests.data.follow.legacy.UnfollowPlaylistRequest;
import com.timwi.challenge.backend.requests.data.library.*;
import com.timwi.challenge.backend.requests.data.personalization.simplified.GetUsersTopArtistsRequest;
import com.timwi.challenge.backend.requests.data.personalization.simplified.GetUsersTopTracksRequest;
import com.timwi.challenge.backend.requests.data.playlists.*;
import com.timwi.challenge.backend.requests.data.search.SearchItemRequest;
import com.timwi.challenge.backend.requests.data.search.simplified.*;
import com.timwi.challenge.backend.requests.data.search.simplified.special.SearchAlbumsSpecialRequest;
import com.timwi.challenge.backend.requests.data.tracks.*;
import com.timwi.challenge.backend.requests.data.users_profile.GetCurrentUsersProfileRequest;
import com.timwi.challenge.backend.requests.data.users_profile.GetUsersProfileRequest;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * Instances of the SpotifyApi class provide access to the Spotify Web API.
 */
public class SpotifyApi {

  /**
   * The default authentication host of Spotify API calls.
   */
  public static final String DEFAULT_AUTHENTICATION_HOST = "accounts.spotify.com";

  /**
   * The default authentication port of Spotify API calls.
   */
  public static final int DEFAULT_AUTHENTICATION_PORT = 443;

  /**
   * The default authentication http scheme of Spotify API calls.
   */
  public static final String DEFAULT_AUTHENTICATION_SCHEME = "https";

  /**
   * The default host of Spotify API calls.
   */
  public static final String DEFAULT_HOST = "api.spotify.com";

  /**
   * A HttpManager configured with default settings.
   */
  public static final IHttpManager DEFAULT_HTTP_MANAGER = new SpotifyHttpManager.Builder().build();

  /**
   * The default port of Spotify API calls.
   */
  public static final int DEFAULT_PORT = 443;

  /**
   * The default http scheme of Spotify API calls.
   */
  public static final String DEFAULT_SCHEME = "https";

  public static final Logger LOGGER = Logger.getLogger(SpotifyApi.class.getName());

  /**
   * The date format used by the Spotify Web API. It uses the {@code GMT}  timezone and the following pattern:
   * {@code yyyy-MM-dd'T'HH:mm:ss}
   */
  private static final ThreadLocal<SimpleDateFormat> SIMPLE_DATE_FORMAT = ThreadLocal.withInitial(() -> makeSimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", "GMT"));

  private final IHttpManager httpManager;
  private final String scheme;
  private final String host;
  private final Integer port;
  private final String proxyUrl;
  private final Integer proxyPort;
  private final Integer proxyUsername;
  private final Integer proxyPassword;
  private final String clientId;
  private final String clientSecret;
  private final URI redirectUri;
  private String accessToken;
  private String refreshToken;

  private SpotifyApi(Builder builder) {
    assert (builder.httpManager != null);

    this.httpManager = builder.httpManager;
    this.scheme = builder.scheme;
    this.host = builder.host;
    this.port = builder.port;
    this.proxyUrl = builder.proxyUrl;
    this.proxyPort = builder.proxyPort;
    this.proxyUsername = builder.proxyUsername;
    this.proxyPassword = builder.proxyPassword;
    this.clientId = builder.clientId;
    this.clientSecret = builder.clientSecret;
    this.redirectUri = builder.redirectUri;
    this.accessToken = builder.accessToken;
    this.refreshToken = builder.refreshToken;
  }

  /**
   * Create a builder for building a new Spotify API instance.
   *
   * @return A {@link Builder}.
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * String concatenation helper method.
   *
   * @param parts     String parts.
   * @param character Separation character.
   * @return A string.
   */
  public static String concat(String[] parts, char character) {
    StringBuilder stringBuilder = new StringBuilder();

    for (String part : parts) {
      stringBuilder.append(part).append(character);
    }

    stringBuilder.deleteCharAt(stringBuilder.length() - 1);

    return stringBuilder.toString();
  }

  /**
   * Parses a date in the default spotify format.
   *
   * @param date the input date to parse
   * @return the pared {@link Date}
   * @throws ParseException if the date is not in a valid format
   */
  public static Date parseDefaultDate(String date) throws ParseException {
    return SIMPLE_DATE_FORMAT.get().parse(date);
  }

  /**
   * Formats a date, using the default spotify format.
   *
   * @param date the date to format
   * @return the formatted date
   */
  public static String formatDefaultDate(Date date) {
    return SIMPLE_DATE_FORMAT.get().format(date);
  }

  public static SimpleDateFormat makeSimpleDateFormat(String pattern, String id) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone(id));

    return simpleDateFormat;
  }

  /**
   * Get the {@link IHttpManager} used for API calls.
   *
   * @return An {@link IHttpManager}.
   */
  public IHttpManager getHttpManager() {
    return httpManager;
  }

  /**
   * Get the scheme used for API calls. Default: {@code https}
   *
   * @return A scheme.
   */
  public String getScheme() {
    return scheme;
  }

  /**
   * Get the API host used for API calls. Default: {@code "api.spotify.com"}
   *
   * @return The host address.
   */
  public String getHost() {
    return host;
  }

  /**
   * Get the port used for API calls. Default: {@code 443}
   *
   * @return A port.
   */
  public Integer getPort() {
    return port;
  }

  /**
   * Get the proxy URL used for API calls.
   *
   * @return The proxy URL.
   */
  public String getProxyUrl() {
    return proxyUrl;
  }

  /**
   * Get the proxy port used for API calls.
   *
   * @return The proxy port.
   */
  public Integer getProxyPort() {
    return proxyPort;
  }

  /**
   * Get the proxy username used for API calls.
   *
   * @return The proxy username.
   */
  public Integer getProxyUsername() {
    return proxyUsername;
  }

  /**
   * Get the proxy password used for API calls.
   *
   * @return The proxy password.
   */
  public Integer getProxyPassword() {
    return proxyPassword;
  }

  /**
   * Get the application client ID specified in this API object.
   *
   * @return Application client ID.
   */
  public String getClientId() {
    return clientId;
  }

  /**
   * Get the application client secret specified in this API object.
   *
   * @return Application client secret.
   */
  public String getClientSecret() {
    return clientSecret;
  }

  /**
   * Get the redirect URI of the application specified in this API object.
   *
   * @return Application redirect URI.
   */
  public URI getRedirectURI() {
    return redirectUri;
  }

  /**
   * Get the access token specified in the API object, which is used for API calls.
   *
   * @return A Spotify Web API access token.
   */
  public String getAccessToken() {
    return accessToken;
  }

  /**
   * Set the access token of the API object.
   *
   * @param accessToken A Spotify Web API access token.
   */
  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  /**
   * Get the refresh token specified in the API object.
   *
   * @return A Spotify Web API refresh token.
   */
  public String getRefreshToken() {
    return refreshToken;
  }

  /**
   * Set the refresh token of the API object.
   *
   * @param refreshToken A Spotify Web API refresh token.
   */
  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  /**
   * Refresh the access token by using authorization code grant. <br>
   * Requires client ID, client secret, and refresh token to be set.
   *
   * @param client_id     When you register your application, Spotify provides you a Client ID.
   * @param client_secret When you register your application, Spotify provides you a Client Secret.
   * @param refresh_token The refresh token returned from the authorization code exchange.
   * @return An {@link AuthorizationCodeRequest.Builder}.
   */
  public AuthorizationCodeRefreshRequest.Builder authorizationCodeRefresh(String client_id, String client_secret, String refresh_token) {
    return new AuthorizationCodeRefreshRequest.Builder(client_id, client_secret)
      .setDefaults(httpManager, scheme, host, port)
      .grant_type("refresh_token")
      .refresh_token(refresh_token);
  }

  /**
   * Refresh the access token by using authorization code grant.
   *
   * @return An {@link AuthorizationCodeRequest.Builder}.
   */
  public AuthorizationCodeRefreshRequest.Builder authorizationCodeRefresh() {
    return new AuthorizationCodeRefreshRequest.Builder(clientId, clientSecret)
      .setDefaults(httpManager, scheme, host, port)
      .grant_type("refresh_token")
      .refresh_token(refreshToken);
  }

  /**
   * Refresh the access token by using the authorization code flow with Proof Key for Code Exchange (PKCE). <br>
   * Requires client ID and refresh token to be set.
   *
   * @param client_id     When you register your application, Spotify provides you a Client ID.
   * @param refresh_token The refresh token returned from the authorization code exchange or the last access token refresh.
   * @return An {@link AuthorizationCodePKCERefreshRequest.Builder}.
   */
  public AuthorizationCodePKCERefreshRequest.Builder authorizationCodePKCERefresh(String client_id, String refresh_token) {
    return new AuthorizationCodePKCERefreshRequest.Builder()
      .setDefaults(httpManager, scheme, host, port)
      .client_id(client_id)
      .grant_type("refresh_token")
      .refresh_token(refresh_token);
  }

  /**
   * Refresh the access token by using the authorization code flow with Proof Key for Code Exchange (PKCE).
   *
   * @return An {@link AuthorizationCodePKCERefreshRequest.Builder}.
   */
  public AuthorizationCodePKCERefreshRequest.Builder authorizationCodePKCERefresh() {
    return new AuthorizationCodePKCERefreshRequest.Builder()
      .setDefaults(httpManager, scheme, host, port)
      .client_id(clientId)
      .grant_type("refresh_token")
      .refresh_token(refreshToken);
  }

  /**
   * Returns a builder that can be used to build requests for authorization code grants. <br>
   * Requires client ID, client secret, authorization code and redirect URI to be set.
   *
   * @param client_id     When you register your application, Spotify provides you a Client ID.
   * @param client_secret When you register your application, Spotify provides you a Client Secret.
   * @param code          The authorization code returned from the initial request to the Account /authorize endpoint.
   * @param redirect_uri  This parameter is used for validation only (there is no actual redirection). The
   *                      value of this parameter must exactly match the value of redirect_uri supplied when requesting
   *                      the authorization code.
   * @return An {@link AuthorizationCodeRequest.Builder}.
   */
  public AuthorizationCodeRequest.Builder authorizationCode(String client_id, String client_secret, String code, URI redirect_uri) {
    return new AuthorizationCodeRequest.Builder(client_id, client_secret)
      .setDefaults(httpManager, scheme, host, port)
      .grant_type("authorization_code")
      .code(code)
      .redirect_uri(redirect_uri);
  }

  /**
   * Returns a builder that can be used to build requests for authorization code grants. <br>
   * Requires authorization code to be set.
   *
   * @param code The authorization code returned from the initial request to the Account /authorize endpoint.
   * @return An {@link AuthorizationCodeRequest.Builder}.
   */
  public AuthorizationCodeRequest.Builder authorizationCode(String code) {
    return new AuthorizationCodeRequest.Builder(clientId, clientSecret)
      .setDefaults(httpManager, scheme, host, port)
      .grant_type("authorization_code")
      .code(code)
      .redirect_uri(redirectUri);
  }
  /**
   * Returns a builder that can be used to build requests for authorization code grants using the Proof Key for Code Exchange (PKCE) flow. <br>
   * Requires client ID, authorization code, code verifier and redirect URI to be set.
   *
   * @param client_id     When you register your application, Spotify provides you a Client ID.
   * @param code          The authorization code returned from the initial request to the Account /authorize endpoint.
   * @param code_verifier The value of this parameter must match the value of the code_verifier that your app generated beforehand.
   * @param redirect_uri  This parameter is used for validation only (there is no actual redirection). The
   *                      value of this parameter must exactly match the value of redirect_uri supplied when requesting
   *                      the authorization code.
   * @return An {@link AuthorizationCodePKCERequest.Builder}.
   * @see <a href="https://developer.spotify.com/documentation/general/guides/authorization-guide/#authorization-code-flow-with-proof-key-for-code-exchange-pkce">
   *      Authorization Code Flow with Proof Key for Code Exchange (PKCE)</a>
   */
  public AuthorizationCodePKCERequest.Builder authorizationCodePKCE(String client_id, String code, String code_verifier, URI redirect_uri) {
    return new AuthorizationCodePKCERequest.Builder()
      .setDefaults(httpManager, scheme, host, port)
      .client_id(client_id)
      .code_verifier(code_verifier)
      .grant_type("authorization_code")
      .code(code)
      .redirect_uri(redirect_uri);
  }

  /**
   * Returns a builder that can be used to build requests for authorization code grants using the Proof Key for Code Exchange (PKCE) flow. <br>
   * Requires authorization code and code verifier to be set.
   *
   * @param code The authorization code returned from the initial request to the Account /authorize endpoint.
   * @param code_verifier The value of this parameter must match the value of the code_verifier that your app generated beforehand.
   * @return An {@link AuthorizationCodePKCERequest.Builder}.
   * @see <a href="https://developer.spotify.com/documentation/general/guides/authorization-guide/#authorization-code-flow-with-proof-key-for-code-exchange-pkce">
   *      Authorization Code Flow with Proof Key for Code Exchange (PKCE)</a>
   */
  public AuthorizationCodePKCERequest.Builder authorizationCodePKCE(String code, String code_verifier) {
    return new AuthorizationCodePKCERequest.Builder()
      .setDefaults(httpManager, scheme, host, port)
      .client_id(clientId)
      .code_verifier(code_verifier)
      .grant_type("authorization_code")
      .code(code)
      .redirect_uri(redirectUri);
  }

  /**
   * Retrieve an URL where the user can give the application permissions.
   *
   * @param client_id    When you register your application, Spotify provides you a Client ID.
   * @param redirect_uri This parameter is used for validation only (there is no actual redirection). The
   *                     value of this parameter must exactly match the value of redirect_uri supplied when requesting
   *                     the authorization code.
   * @return An {@link AuthorizationCodeUriRequest.Builder}.
   */
  public AuthorizationCodeUriRequest.Builder authorizationCodeUri(String client_id, URI redirect_uri) {
    return new AuthorizationCodeUriRequest.Builder()
      .setDefaults(httpManager, scheme, host, port)
      .client_id(client_id)
      .response_type("code")
      .redirect_uri(redirect_uri);
  }

  /**
   * Retrieve an URL where the user can give the application permissions.
   *
   * @return An {@link AuthorizationCodeUriRequest.Builder}.
   */
  public AuthorizationCodeUriRequest.Builder authorizationCodeUri() {
    return new AuthorizationCodeUriRequest.Builder()
      .setDefaults(httpManager, scheme, host, port)
      .client_id(clientId)
      .response_type("code")
      .redirect_uri(redirectUri);
  }

  /**
   * Retrieve an URL where the user can give the application permissions using the Proof Key for Code Exchange (PKCE) flow.
   *
   * @param client_id    When you register your application, Spotify provides you a Client ID.
   * @param code_challenge  The code challenge that your app calculated beforehand.
   *                        The code challenge is the base64url encoded sha256-hash of the code verifier,
   *                        which is a cryptographically random string between 43 and 128 characters in length.
   *                        It can contain letters, digits, underscores, periods, hyphens, or tildes and is generated
   *                        by your app before each authentication request.
   * @param redirect_uri This parameter is used for validation only (there is no actual redirection). The
   *                     value of this parameter must exactly match the value of redirect_uri supplied when requesting
   *                     the authorization code.
   * @return An {@link AuthorizationCodeUriRequest.Builder}.
   * @see <a href="https://developer.spotify.com/documentation/general/guides/authorization-guide/#authorization-code-flow-with-proof-key-for-code-exchange-pkce">
   *      Authorization Code Flow with Proof Key for Code Exchange (PKCE)</a>
   */
  public AuthorizationCodeUriRequest.Builder authorizationCodePKCEUri(String client_id, String code_challenge, URI redirect_uri) {
    return new AuthorizationCodeUriRequest.Builder()
      .setDefaults(httpManager, scheme, host, port)
      .client_id(client_id)
      .response_type("code")
      .code_challenge_method("S256")
      .code_challenge(code_challenge)
      .redirect_uri(redirect_uri);
  }

  /**
   * Retrieve an URL where the user can give the application permissions using the Proof Key for Code Exchange (PKCE) flow.
   *
   * @param code_challenge  The code challenge that your app calculated beforehand.
   *                        The code challenge is the base64url encoded sha256-hash of the code verifier,
   *                        which is a cryptographically random string between 43 and 128 characters in length.
   *                        It can contain letters, digits, underscores, periods, hyphens, or tildes and is generated
   *
   * @return An {@link AuthorizationCodeUriRequest.Builder}.
   * @see <a href="https://developer.spotify.com/documentation/general/guides/authorization-guide/#authorization-code-flow-with-proof-key-for-code-exchange-pkce">
   *      Authorization Code Flow with Proof Key for Code Exchange (PKCE)</a>
   */
  public AuthorizationCodeUriRequest.Builder authorizationCodePKCEUri(String code_challenge) {
    return new AuthorizationCodeUriRequest.Builder()
      .setDefaults(httpManager, scheme, host, port)
      .client_id(clientId)
      .response_type("code")
      .code_challenge_method("S256")
      .code_challenge(code_challenge)
      .redirect_uri(redirectUri);
  }

  /**
   * Returns a builder that can be used to build requests for client credential grants. <br>
   * Requires client ID and client secret to be set.
   *
   * @return A {@link ClientCredentialsRequest.Builder}.
   */
  public ClientCredentialsRequest.Builder clientCredentials() {
    return new ClientCredentialsRequest.Builder(clientId, clientSecret)
      .setDefaults(httpManager, scheme, host, port)
      .grant_type("client_credentials");
  }

  /**
   * Returns an album with the ID given below.
   *
   * @param id The Spotify album ID of the album you're trying to retrieve.
   * @return A {@link GetAlbumRequest.Builder}.
   * @see <a href="https://developer.spotify.com/web-api/user-guide/#spotify-uris-and-ids">Spotify: URLs &amp; IDs</a>
   */
  public GetAlbumRequest.Builder getAlbum(String id) {
    return new GetAlbumRequest.Builder(accessToken)
      .setDefaults(httpManager, scheme, host, port)
      .id(id);
  }

  /**
   * Returns the tracks of the album with the ID given below.
   *
   * @param id The Spotify ID of the album you're trying to retrieve.
   * @return A {@link GetAlbumsTracksRequest.Builder}.
   * @see <a href="https://developer.spotify.com/web-api/user-guide/#spotify-uris-and-ids">Spotify: URLs &amp; IDs</a>
   */
  public GetAlbumsTracksRequest.Builder getAlbumsTracks(String id) {
    return new GetAlbumsTracksRequest.Builder(accessToken)
      .setDefaults(httpManager, scheme, host, port)
      .id(id);
  }

  /**
   * Get multiple albums.
   *
   * @param ids The Spotify IDs of all albums you're trying to retrieve. Maximum: 20 IDs.
   * @return A {@link GetSeveralAlbumsRequest.Builder}.
   * @see <a href="https://developer.spotify.com/web-api/user-guide/#spotify-uris-and-ids">Spotify: URLs &amp; IDs</a>
   */
  public GetSeveralAlbumsRequest.Builder getSeveralAlbums(String... ids) {
    return new GetSeveralAlbumsRequest.Builder(accessToken)
      .setDefaults(httpManager, scheme, host, port)
      .ids(concat(ids, ','));
  }


  /**
   * Get a list of the albums saved in the current Spotify user’s "Your Music" library.
   *
   * @return A {@link GetCurrentUsersSavedAlbumsRequest.Builder}.
   */
  public GetCurrentUsersSavedAlbumsRequest.Builder getCurrentUsersSavedAlbums() {
    return new GetCurrentUsersSavedAlbumsRequest.Builder(accessToken)
      .setDefaults(httpManager, scheme, host, port);
  }


  /**
   * Remove one or more albums from the current user's "Your Music" library.
   *
   * @param ids A list of the Spotify IDs. Maximum: 50 IDs.
   * @return A {@link RemoveAlbumsForCurrentUserRequest.Builder}.
   * @see <a href="https://developer.spotify.com/web-api/user-guide/#spotify-uris-and-ids">Spotify: URLs &amp; IDs</a>
   */
  public RemoveAlbumsForCurrentUserRequest.Builder removeAlbumsForCurrentUser(String... ids) {
    return new RemoveAlbumsForCurrentUserRequest.Builder(accessToken)
      .setDefaults(httpManager, scheme, host, port)
      .ids(concat(ids, ','));
  }

  /**
   * Remove one or more albums from the current user's "Your Music" library.
   *
   * @param ids The Spotify IDs for the albums to be deleted. Maximum: 50 IDs.
   * @return A {@link RemoveAlbumsForCurrentUserRequest.Builder}.
   * @see <a href="https://developer.spotify.com/web-api/user-guide/#spotify-uris-and-ids">Spotify: URLs &amp; IDs</a>
   */
  public RemoveAlbumsForCurrentUserRequest.Builder removeAlbumsForCurrentUser(JsonArray ids) {
    return new RemoveAlbumsForCurrentUserRequest.Builder(accessToken)
      .setDefaults(httpManager, scheme, host, port)
      .ids(ids);
  }

 

  /**
   * Builder class for building {@link SpotifyApi} instances.
   */
  public static class Builder {

    private IHttpManager httpManager = DEFAULT_HTTP_MANAGER;
    private String scheme = DEFAULT_SCHEME;
    private String host = DEFAULT_HOST;
    private Integer port = DEFAULT_PORT;
    private String proxyUrl;
    private Integer proxyPort;
    private Integer proxyUsername;
    private Integer proxyPassword;
    private String clientId;
    private String clientSecret;
    private URI redirectUri;
    private String accessToken;
    private String refreshToken;

    /**
     * The HttpManager setter.
     *
     * @param httpManager A Spotify HttpManager.
     * @return A {@link Builder}.
     */
    public Builder setHttpManager(IHttpManager httpManager) {
      this.httpManager = httpManager;
      return this;
    }

    /**
     * The scheme setter.
     *
     * @param scheme A HTTP-scheme.
     * @return A {@link Builder}.
     */
    public Builder setScheme(String scheme) {
      this.scheme = scheme;
      return this;
    }

    /**
     * The Spotify API host setter.
     *
     * @param host A Spotify API host.
     * @return A {@link Builder}.
     */
    public Builder setHost(String host) {
      this.host = host;
      return this;
    }

    /**
     * The Spotify API port setter.
     *
     * @param port A Spotify API port.
     * @return A {@link Builder}.
     */
    public Builder setPort(Integer port) {
      this.port = port;
      return this;
    }

    /**
     * The proxy URL setter.
     *
     * @param proxyUrl A proxy URL.
     * @return A {@link Builder}.
     */
    public Builder setProxyUrl(String proxyUrl) {
      this.proxyUrl = proxyUrl;
      return this;
    }

    /**
     * The proxy port setter.
     *
     * @param proxyPort A proxy port.
     * @return A {@link Builder}.
     */
    public Builder setProxyPort(Integer proxyPort) {
      this.proxyPort = proxyPort;
      return this;
    }

    /**
     * The proxy username setter.
     *
     * @param proxyUsername A proxy username.
     * @return A {@link Builder}.
     */
    public Builder setProxyUsername(Integer proxyUsername) {
      this.proxyUsername = proxyUsername;
      return this;
    }

    /**
     * The proxy password setter.
     *
     * @param proxyPassword A proxy password.
     * @return A {@link Builder}.
     */
    public Builder setProxyPassword(Integer proxyPassword) {
      this.proxyPassword = proxyPassword;
      return this;
    }

    /**
     * The client ID setter.
     *
     * @param clientId A client ID of your application.
     * @return A {@link Builder}.
     */
    public Builder setClientId(String clientId) {
      this.clientId = clientId;
      return this;
    }

    /**
     * The client secret setter.
     *
     * @param clientSecret A client secret of your application.
     * @return A {@link Builder}.
     */
    public Builder setClientSecret(String clientSecret) {
      this.clientSecret = clientSecret;
      return this;
    }

    /**
     * The redirect URI setter.
     *
     * @param redirectUri A redirect URI of your application.
     * @return A {@link Builder}.
     */
    public Builder setRedirectUri(URI redirectUri) {
      this.redirectUri = redirectUri;
      return this;
    }

    /**
     * The access token setter.
     *
     * @param accessToken A Spotify API access token.
     * @return A {@link Builder}.
     */
    public Builder setAccessToken(String accessToken) {
      this.accessToken = accessToken;
      return this;
    }

    /**
     * The refresh token setter.
     *
     * @param refreshToken A Spotify API refresh token.
     * @return A {@link Builder}.
     */
    public Builder setRefreshToken(String refreshToken) {
      this.refreshToken = refreshToken;
      return this;
    }

    /**
     * Build a {@link SpotifyApi} instance with the information given to the builder.
     *
     * @return A {@link SpotifyApi} instance.
     */
    public SpotifyApi build() {
      return new SpotifyApi(this);
    }
  }
}

