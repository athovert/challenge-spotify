package com.timwi.challenge.backend.requests.data;

import com.timwi.challenge.backend.model_objects.specification.PagingCursorbased;
import com.timwi.challenge.backend.requests.IRequest;

public interface IPagingCursorbasedRequestBuilder<T, A, BT extends IRequest.Builder<PagingCursorbased<T>, ?>>
  extends IRequest.Builder<PagingCursorbased<T>, BT> {
  BT limit(final Integer limit);

  BT after(final A after);
}
