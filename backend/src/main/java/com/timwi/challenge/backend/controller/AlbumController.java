package com.timwi.challenge.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.timwi.challenge.backend.SpotifyApi;
import com.timwi.challenge.backend.model_objects.specification.Album;
import com.timwi.challenge.backend.requests.data.albums.GetAlbumRequest;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class AlbumController {
	
	private Logger logger = LoggerFactory.getLogger(AlbumController.class);
	
	@GetMapping("/album/{id}")
	public ResponseEntity<Object> getAlbumById(@PathVariable("id") String id) {
		
		try {
			SpotifyApi spotifyApi = new SpotifyApi.Builder()
        .setAccessToken("BQAzricRinjlsWmMgTjMh4Jb5QA6p5A6vJA2U7Qhf1McoXsEgjfyWSUh2H8q299FbvoFvcpmlEJl9kG-vxDacb_FiL-FaF2AqOFvmzwWYCLasIoRJpHz278e2lu9V--0XJWH0amPvptVtT0Qt_Vt3BTiYQ2aqKwsJY4")
        .build();
		
			final GetAlbumRequest getAlbumRequest = spotifyApi.getAlbum(id)
			.build();
			
			final Album album = getAlbumRequest.execute();
			
			if(album != null) {
				return new ResponseEntity<Object>(album, HttpStatus.OK);				
			} else {
				return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
			}
		}
		
		catch(Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
		}
	}
	
}