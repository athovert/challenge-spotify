package com.timwi.challenge.backend.requests.data;

import com.timwi.challenge.backend.model_objects.specification.Paging;
import com.timwi.challenge.backend.requests.IRequest;

public interface IPagingRequestBuilder<T, BT extends IRequest.Builder<Paging<T>, ?>>
  extends IRequest.Builder<Paging<T>, BT> {
  BT limit(final Integer limit);

  BT offset(final Integer offset);
}
