package com.timwi.challenge.backend.requests.data.personalization.interfaces;

import com.timwi.challenge.backend.model_objects.IModelObject;

public interface IArtistTrackModelObject extends IModelObject {
}
