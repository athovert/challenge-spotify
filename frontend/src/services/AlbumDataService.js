import http from '../http-common'

class AlbumDataService {
    getAll() {
        return http.get('/albums')
    }

    get(id) {
        return http.get(`/albums/${id}`)
    }

}

export default new AlbumDataService()
